// #ifdef _OPENMP
    #include <omp.h>
// #endif
#include <iostream>
#include <CStopWatch.h>

double f(double x){
    double retVal = 0;

    retVal = x*x;

    return retVal;
}

double Trap(double a, double b, double n){
    double h, retValue;

    h = (b-a)/n;
    retValue = (f(a)+f(b))/2;

    int m = n;
    #pragma omp parallel for reduction(+: retValue)
    for (int i = 1; i <= m-1; i++){
        retValue += f(a + i * h);
    }

    retValue = h*retValue;

    return retValue;
}
int main(){

    double a = 0, b = 16, n = 10000;
    double result = 0.0;
    double numThreads = 1, endThreads = 12;
    CStopWatch timer;
    
    std::cout << "NumThreads, a, n, Result, Time\n";
    
    for(numThreads = 1; numThreads<=endThreads; numThreads++) {
        for (a = 0; a <= 16; a++) {
            for (n = 10000; n <= 100000; n += 1000) {
                omp_set_num_threads(numThreads);
                
                timer.startTimer();
                result = Trap(a, b, n);
                timer.stopTimer();

                std::cout << numThreads << "," << a << "," << n << "," << result << "," << timer.getElapsedTime() << "\n";
            }
        }
    }

    // numThreads = 1;
    // omp_set_num_threads(numThreads);
    // result = Trap(a, b, n);
    // std::cout << "Serial Result: " << result << "\n";

    // numThreads = 4;
    // omp_set_num_threads(numThreads);
    // result = Trap(a, b, n);
    // std::cout << "Parallel Result: " << result << "\n";

    return 0;
}
